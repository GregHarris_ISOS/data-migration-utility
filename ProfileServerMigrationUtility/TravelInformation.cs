namespace ProfileServerMigrationUtility
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TravelInformation : DbContext
    {
        public TravelInformation()
            : base("name=TravelInformationConnection")
        {
        }

        public virtual DbSet<CustomerSetting> CustomerSettings { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CustomerSettings)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);
        }
    }
}
