﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;
using ProfileServerMigrationUtility.Models;
using System.Diagnostics;
using System.Configuration;
using ISOS.SystemLoggerSDK;
using ProfileServerMigrationUtility.Common;

namespace ProfileServerMigrationUtility
{
    class Program
    {
        public static int pollingInterval = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PollingInterval"));
        public static bool isMigrating = false;
        private static readonly string AppName = ConfigurationManager.AppSettings.Get("ApplicationName");
        public LogItem LogItem { get; set; } = new LogItem() { LogLevel = ISOS.SystemLoggerSDK.LogLevel.Trace };

        static void Main(string[] args)
        {
            Console.WriteLine("=============================================================================");
            Console.WriteLine("                           PROFILE SYNC UTILITY                              ");
            Console.WriteLine("=============================================================================");
            Console.WriteLine("Version {0}", ConfigurationManager.AppSettings.Get("Version"));
            Console.WriteLine("This utility will sync profiles from Middleware/Okta to Traveltracker database for a Membership Number");

            Logger.SystemLogger.Log("Starting Profile Sync Utility Version {0}", ConfigurationManager.AppSettings.Get("Version"));

            try
            {
                while (true)
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IncludeCheckForModifiedCustomers")))
                    {
                        CheckForChangedMemberships();
                    }

                    if (!isMigrating)
                        CheckForJobs();

                    Logger.SystemLogger.Log("Sleeping...");

                    Logger.SystemLogger.FlushBuffer();

                    // Wait for polling interval converted to milliseconds
                    Thread.Sleep(pollingInterval * 60 * 1000);
                }
            }
            catch (Exception ex)
            {
                throw new UnauthorizedAccessException(ex.Message);
            }
            finally
            {
                Logger.SystemLogger.FlushBuffer();
            }
        }

        private static void CheckForChangedMemberships()
        {
            Logger.SystemLogger.Log("Time to check for any changed TT memberships");

            DateTime lastCheck;

            //Determine the last time we inserted into the Sync Job table (presumably based on changes in TT)
            using (var dbMobileMiddleware = new MobileMiddleware())
            {
                //Default to now (assuming app server time == db server time)
                lastCheck = DateTime.Now;

                var queryMw = (from b in dbMobileMiddleware.ProfileSyncJobs
                               orderby b.InsertDate descending
                               select b).Take(1);

                if (queryMw.Any())
                {
                    var date = queryMw.Single().InsertDate;

                    //Determine whether it's time to check again
                    lastCheck =
                        date.AddMinutes(
                            Convert.ToInt32(ConfigurationManager.AppSettings.Get("AutomationIntervalInMinutes")));
                }
            }

#if DEBUG
            Console.WriteLine(lastCheck);

            lastCheck = new DateTime(2017, 7, 30);
#endif
            //If it's time to queue up jobs, get Membership Number and Status of all accounts changed in TT since the last datetime we checked
            if (lastCheck < DateTime.Now)
            {
                QueueUpSyncJobs(lastCheck);
            }
        }

        private static void QueueUpSyncJobs(DateTime lastCheck)
        {
            Logger.SystemLogger.Log(string.Format("Looking for any changed TT memberships since last check at {0}..", string.Format("{0:G}", lastCheck)));

            using (var dbTravelInfo = new TravelInformation())
            {
                //Looking for the membership number and the Customer
                var changedMembershipCollection = dbTravelInfo.CustomerSettings.Join(
                    dbTravelInfo.Customers,
                    settings => settings.iCustomerId,
                    customer => customer.iCustomerID,
                    (setting, customer) =>
                        new
                        {
                            MembershipNumber = setting.nvchTTMembershipNumber,
                            TravelTrackerActivated = customer.bActive ?? false,
                            LastModified = setting.dtModified
                        })
                    .Where(t => t.LastModified > lastCheck)
                    .GroupBy(c => new { c.MembershipNumber, c.TravelTrackerActivated })
                    .Select(m => new
                    {
                        m.FirstOrDefault().MembershipNumber,
                        m.FirstOrDefault().TravelTrackerActivated,
                        m.FirstOrDefault().LastModified,
                        rn = m.Count()
                    })
                    .Where(t => t.rn == 1)
                    .AsEnumerable()
                    .Select(n => new
                    {
                        n.MembershipNumber,
                        n.TravelTrackerActivated,
                        n.LastModified
                    })
                    .OrderBy(s => s.LastModified)
                    ;

                foreach (var c in changedMembershipCollection)
                {
                    Logger.SystemLogger.Log(c.MembershipNumber +
                                " was set (but not necessarily changed) to " +
                                (c.TravelTrackerActivated ? "Active" : "Inactive") + " on " +
                                string.Format("{0:G}", c.LastModified));
                }

                var membershipNumberList =
                    changedMembershipCollection.Select(c => c.MembershipNumber).ToList();

                //Find any memberships that already in the queue and are set to be (or most recently have been) migrated in the same direction. I.e., the intention is that, if XYZ was in the queue and was most recently processed to be migrated to TT, and has again been recently modified in TT, but is still active in TT, then don't re-insert it into the queue. If, on the other hand, it is no longer active in TT, then insert it in the queue as needing to be migrated back to Okta.
                using (var dbMobileMiddleware = new MobileMiddleware())
                {
                    //Find the most recent record in the queue for each of the TT memberships that have recently changed
                    var queuedMemberships =
                        dbMobileMiddleware.ProfileSyncJobs.Select(
                            a => new { a.MembershipNumber, a.ActivateMembership, a.InsertDate })
                            .Where(
                                b => membershipNumberList.Contains(b.MembershipNumber))
                            .GroupBy(d => d.MembershipNumber)
                            .Select(e => e.OrderByDescending(s => s.InsertDate).FirstOrDefault())
                            .Select(m => new
                            {
                                m.MembershipNumber,
                                m.ActivateMembership,
                                m.InsertDate
                            });

                    foreach (var i in queuedMemberships)
                    {
                        Logger.SystemLogger.Log(string.Format("{0} was last inserted to the Queue as {1} on {2}",
                            i.MembershipNumber, i.ActivateMembership.ToString(), string.Format("{0:G}", i.InsertDate)));
                    }

                    //Remove any exact matches from the list of recently changed memberships in TT 
                    var filteredChangedMembershipCollection = changedMembershipCollection
                        .Select(
                            a => new { memNum = a.MembershipNumber, activated = a.TravelTrackerActivated })
                        .Except(
                            queuedMemberships.Select(
                                b => new { memNum = b.MembershipNumber, activated = b.ActivateMembership }));

                    foreach (var i in filteredChangedMembershipCollection)
                    {
                        Logger.SystemLogger.Log("{0} will now be inserted as ActivateMembership: {1}", i.memNum, i.activated.ToString());

                        var jobEmailAddress = ConfigurationManager.AppSettings.Get("JobEmailAddress");

                        dbMobileMiddleware.ProfileSyncJobs.Add(new ProfileSyncJob()
                        {
                            ActivateMembership = i.activated,
                            EmailID = jobEmailAddress,
                            InsertDate = DateTime.Now,
                            MembershipNumber = i.memNum,
                            SyncStatus = ConfigurationManager.AppSettings.Get("SyncStatusForInsert")
                        });
                    }

                    Logger.SystemLogger.Log("Done queuing up any changed TT memberships");

                    dbMobileMiddleware.SaveChanges();
                }
            }

            Logger.SystemLogger.FlushBuffer();
        }

        private static void CheckForJobs()
        {
            Logger.SystemLogger.Log("Checking for waiting jobs...");

            List<ProfileSyncJob> syncJobs = Database.SelectProfileSyncJob();

            foreach (ProfileSyncJob syncJob in syncJobs)
            {
                Logger.SystemLogger.Log(string.Format("Processing job {0} for membership #: {1}", syncJob.SyncJobID, syncJob.MembershipNumber));

                isMigrating = true;
                if (syncJob.ActivateMembership)
                {
                    IdamToTT ap = new IdamToTT(syncJob);
                    ap.StartJob();
                }
                else
                {
                    TTtoIdam dap = new TTtoIdam(syncJob);
                    dap.StartJob();
                }

                isMigrating = false;
            }

            Logger.SystemLogger.Log("Finished checking for waiting jobs...");
        }
    }
}
