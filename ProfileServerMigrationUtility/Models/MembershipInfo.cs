﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility.Models
{
    public class GetMembershipInfoRequest
    {
        public int deviceId { get; set; }
        public string membershipNumber { get; set; }
        public string locale { get; set; }
        public string appVersion { get; set; }
    }
    public class GetMembershipInfoResponse
    {
        public string StatusCode { get; set; }
        public MembershipInfo MembershipInfo { get; set; }
        public List<CustomMenu> CustomMenu { get; set; }

    }
    public class MembershipInfo
    {
        public MembershipInfo()
        {

            CompanyResources = null;
        }

        /// <summary>
        /// Client Membership Number
        /// </summary>
        /// <value>The membership number.</value>
        public string MembershipNumber { get; set; }

        public string MembershipType
        {
            get
            {
                if (MedicalInfo && !TravelInfo)
                {
                    return "M";
                }
                else if (!MedicalInfo && TravelInfo)
                {
                    return "S";
                }
                else
                {
                    return "C";
                }
            }
        }


        /// <summary>
        /// The Assistance Center Number for Member
        /// </summary>
        /// <value>The dedicated line number.</value>
        public string DedicatedLineNumber { get; set; }

        /// <summary>
        /// Whether member has access to medical guides
        /// </summary>
        /// <value><c>true</c> if medical guide access; otherwise, <c>false</c>.</value>
        public bool MedicalInfo { get; set; }

        /// <summary>
        /// Whether member has access to travel guides
        /// </summary>
        /// <value><c>true</c> if travel guide access; otherwise, <c>false</c>.</value>
        public bool TravelInfo { get; set; }

        /// <summary>
        /// Whether client has location tracking enabled for their members
        /// </summary>
        /// <value><c>true</c> if location tracking; otherwise, <c>false</c>.</value>
        public bool LocationTracking { get; set; }


        /// <summary>
        /// Whether client has push notifications enabled for their members
        /// </summary>
        /// <value><c>true</c> if push notifications; otherwise, <c>false</c>.</value>
        public bool PushNotifications { get; set; }

        /// <summary>
        /// Whether client has access to Travel Tracker
        /// </summary>
        /// <value><c>true</c> if Travel Tracker; otherwise, <c>false</c>.</value>
        public bool TravelTracker { get; set; }

        /// <summary>
        /// Whether client wants the user to be prompted to create their profile if none
        /// </summary>
        /// <value><c>true</c> if force profile update; otherwise, <c>false</c>.</value>
        public bool ForceProfileUpdate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:ISOS.MobileSDK.Model.MembershipInfo"/> chat enable.
        /// </summary>
        /// <value><c>true</c> if chat enable; otherwise, <c>false</c>.</value>
        public bool ChatEnable { get; set; }

        /// <summary>
        /// Force the user to be logged out
        /// </summary>
        /// <value><c>true</c> to force a logout; otherwise, <c>false</c>.</value>
        public bool ForceLogout { get; set; }

        /// <summary>
        /// Client's company name
        /// </summary>
        /// <value>The name of the company.</value>
        public string CompanyName { get; set; }

        /// <summary>
        /// Link to client's logo
        /// </summary>
        /// <value>The company logo.</value>
        public string CompanyLogo { get; set; }

        public List<CustomMenu> CompanyResources;


        // Replace above with this
        //List<MembershipAttribute> MembershipAttributes { get; set; }


        /// <summary>
        /// Status (Unknow why)
        /// </summary>
        /// <value>MOBILE_NOT_ENABLED, MEMBERSJIP_TYPE_BLANK, OR OK</value>
        public string StatusCode { get; set; }

        /// <summary>
        /// List of Menu Items for this Membership. It will use default if no Custom Menu returned
        /// </summary>
        //public List<CustomMenu> CustomMenu;

        public List<Language> AvailableLanguages;

    }

    public class CustomMenu 
    {
        public int MenuID { get; set; }
        public string MenuTitle { get; set; }
        public string ModuleName { get; set; }
        public string MenuIcon { get; set; }
    }

    public class Language 
    {
        public string name { get; set; }
        public string locale { get; set; }
        public string cultureCode { get; set; }
    }
}
