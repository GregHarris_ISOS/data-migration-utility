﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility.Models
{
    public class LoginUser
    {
        public int LoginId { get; set; }
        public string UserName { get; set; }
        public string MembershipNumber { get; set; }
        public string IndividualId { get; set; }
        public int? DeviceId { get; set; }
        public string Model { get; set; }
        public string OSVersion { get; set; }
        public string AppVersion { get; set; }
    }
}
