﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility.Models
{
    public class CreateUserProfileRequest
    {
        public string membershipId { get; set; }
        public string deviceId { get; set; }
        public string model { get; set; }
        public string osVersion { get; set; }
        public string appVersion { get; set; }
        public string UserName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobileNumberCountryCode { get; set; }
        public string mobileNumber { get; set; }
        public string emailAddress { get; set; }
        public int countryOfResidence { get; set; }
        public string countryOfResidenceIsoCode { get; set; }
        public bool optInToLocationTracking { get; set; }

        public bool? travelTracker { get; set; }
        public string individualId { get; set; }

        private string _mobileNumberCountryCode;
    }

    public class CreateUserProfileResponse
    {
        public string IndividualID { get; set; }
    }

    public class GetUserProfileRequest
    {
        public string membershipId { get; set; }
        public string individualId { get; set; }
        public bool? travelTracker { get; set; }
    }

    public class GetUserProfileResponse
    {
        public profile Profile { get; set; }

        public class profile
        {
            public int CountryOfResidence { get; set; }
            public string EmailAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MobileNumber { get; set; }
            public string MobileNumberCountryCode { get; set; }
            public bool OptInToLocationTracking { get; set; }
            private string _mobileNumberCountryCode;
            public string SecurityQuestionText { get; set; }
            public string SecurityQuestionType { get; set; }
            public DateTime? PasswordUpdatedDate { get; set; }
            public DateTime? SecurityQuestionUpdatedDate { get; set; }
        }
    }
}
