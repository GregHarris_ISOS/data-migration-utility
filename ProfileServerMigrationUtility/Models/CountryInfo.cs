﻿using System.Data;

namespace ProfileServerMigrationUtility.Models
{
    public class CountryInfo
    {
        public CountryInfo() { }
        public CountryInfo(DataRow dr)
        {
            ISOSCountryID = string.IsNullOrEmpty(dr["CountryID"].ToString().Trim()) ? 0 : int.Parse(dr["CountryID"].ToString().Trim());
            CountryName = string.IsNullOrEmpty(dr["CountryName"].ToString().Trim()) ? string.Empty : dr["CountryName"].ToString().Trim();
            TSSCountryID = string.IsNullOrEmpty(dr["TSSCountryID"].ToString().Trim()) ? 0 : int.Parse(dr["TSSCountryID"].ToString().Trim());
            ISOCountryCode = string.IsNullOrEmpty(dr["ISOCountryCode"].ToString().Trim()) ? string.Empty : dr["ISOCountryCode"].ToString().Trim();
        }
        public string CountryName { get; set; }

        public int TSSCountryID { get; set; }

        public int ISOSCountryID { get; set; }

        public string ISOCountryCode { get; set; }
    }
}