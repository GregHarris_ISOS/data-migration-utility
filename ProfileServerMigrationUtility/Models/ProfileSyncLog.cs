﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility.Models
{
    public class ProfileSyncLog
    {
        public int SyncJobID { get; set; }
        public int LoginID { get; set; }
        public int RunNumber { get; set; }
        public string OktaLoginName { get; set; }
        public string OktaFirstName { get; set; }
        public string OktaLastName { get; set; }
        public string OktaEmailId { get; set; }
        public string OktaMobilePhone { get; set; }
        public string OktaCountryCode { get; set; }
        public string OktaUniversalId { get; set; }
        public string SyncStatus { get; set; }
        public string SyncDescription { get; set; }
    }
}
