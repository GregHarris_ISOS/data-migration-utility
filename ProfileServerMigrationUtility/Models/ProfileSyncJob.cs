﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility.Models
{
    public class ProfileSyncJob
    {
        public int SyncJobID { get; set; }
        public string MembershipNumber { get; set; }
        public bool ActivateMembership { get; set; }
    }
}
