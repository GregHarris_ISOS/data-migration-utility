﻿using ISOS.IdentitySDK;
using ISOS.IdentitySDK.Models;
using ISOS.IdentitySDK.Providers;
using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProfileServerMigrationUtility.Common;

namespace ProfileServerMigrationUtility
{
    public class IdamToTT
    {
        private ProfileSyncJob SyncJob { get; set; }
        private bool JobSuccess { get; set; }
        private DataService ds = new DataService();
        private MembershipInfo membershipInfo;

        public IdamToTT(ProfileSyncJob syncjob)
        {
            SyncJob = syncjob;
        }

        public void StartJob()
        {
            Logger.SystemLogger.Log(string.Format("Migrating profiles from Okta to TT for Membership #{0}", SyncJob.MembershipNumber));

            try
            {
                Task.Run(async () =>
                {
                    membershipInfo = await ds.GetMembershipInfo(SyncJob.MembershipNumber);
                }).GetAwaiter().GetResult();

                List<LoginUser> loginUsers = Database.SelectLoginUser(SyncJob.MembershipNumber, SyncJob.SyncJobID);

                if (loginUsers.Any())
                {
                    Logger.SystemLogger.Log(string.Format("Found {0} profiles to migrate", loginUsers.Count));

                    JobSuccess = true;
                    //Get RunNumber from database
                    int iRun = Database.SelectRunNumber(SyncJob.SyncJobID);

                    //Update Status in database
                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.InProgress.ToString());

                    foreach (LoginUser user in loginUsers)
                    {
                        MigrateUser(user, iRun);
                    }

                    string jobReport = Database.SelectJobReport(SyncJob.SyncJobID, iRun);
                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID,
                                                  JobSuccess == true ? Constants.JobStatus.Completed.ToString() : Constants.JobStatus.CompletedWithErrors.ToString(),
                                                  jobReport);                   
                }
                else
                {
                    Logger.SystemLogger.Log(string.Format("No profiles to migrate for {0}", SyncJob.MembershipNumber));

                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.Failed.ToString(),Constants.USERS_NOT_FOUND);
                }

                Logger.SystemLogger.Log(string.Format("Migrating profiles complete for Membership #: {0}", SyncJob.MembershipNumber));
            }
            catch (Exception ex)
            {
                Logger.SystemLogger.Log(string.Format("Error: {0}", ex.Message));

                Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.Failed.ToString(), ex.Message);
            }
        }

        public void MigrateUser(LoginUser user, int iRunNumber)
        {
            Logger.SystemLogger.Log(string.Format("Processing {0}...", user.LoginId));

            ProfileSyncLog userProfile = new ProfileSyncLog();
            userProfile.SyncJobID = SyncJob.SyncJobID;
            userProfile.LoginID = user.LoginId;
            userProfile.RunNumber = iRunNumber;
            userProfile.OktaLoginName = user.UserName;
            try
            {
                //Get Profile from Okta
                IdentityProvider identityProvider = new IdentityProvider();
                IResult<User> userResult=new Result<User>();

                Task.Run(async () =>
                {
                    userResult = await identityProvider.GetUser(user.UserName).ConfigureAwait(false);
                }).GetAwaiter().GetResult();

                if (userResult.IsSuccess && userResult.ReturnValue.Profile.CredentialsOnly == false)
                {
                    Logger.SystemLogger.Log(string.Format("Okta profile retrieved for {0}", user.LoginId));

                    //Call Create Profile Service
                    CreateUserProfileRequest ttUserProfile = new CreateUserProfileRequest();
                    ttUserProfile.travelTracker = true;
                    ttUserProfile.individualId = user.IndividualId;
                    ttUserProfile.membershipId = SyncJob.MembershipNumber;
                    ttUserProfile.UserName = userResult.ReturnValue.Profile.Login;
                    ttUserProfile.firstName = userResult.ReturnValue.Profile.FirstName;
                    ttUserProfile.lastName = userResult.ReturnValue.Profile.LastName;
                    ttUserProfile.mobileNumberCountryCode = userResult.ReturnValue.Profile.MobilePhoneCountryCode;
                    ttUserProfile.mobileNumber = userResult.ReturnValue.Profile.MobilePhone;
                    ttUserProfile.emailAddress = userResult.ReturnValue.Profile.Email;
                    ttUserProfile.countryOfResidence = Database.SelectCountryId( userResult.ReturnValue.Profile.CountryCode);
                    ttUserProfile.optInToLocationTracking = membershipInfo.LocationTracking;

                    ttUserProfile.deviceId = user.DeviceId.ToString();
                    ttUserProfile.model = user.Model;
                    ttUserProfile.osVersion = user.OSVersion;
                    ttUserProfile.appVersion = user.AppVersion;

                    Logger.SystemLogger.Log(string.Format("Creating (or updating) TT profile for {0}", user.LoginId));

                    string ttIndividualId =string.Empty;
                    Task.Run(async () =>
                    {
                        ttIndividualId = await ds.CreateUserProfile(ttUserProfile);
                    }).GetAwaiter().GetResult();

                    Logger.SystemLogger.Log(string.Format("Created (or updated) TT profile for {0}", user.LoginId));

                    //Update User flag in Okta
                    UserProfile oktaProfile = new UserProfile();
                    oktaProfile.Login = userResult.ReturnValue.Profile.Email;
                    oktaProfile.FirstName = userResult.ReturnValue.Profile.FirstName;
                    oktaProfile.LastName = userResult.ReturnValue.Profile.LastName;
                    oktaProfile.Email = userResult.ReturnValue.Profile.Email;
                    oktaProfile.CountryCode = userResult.ReturnValue.Profile.CountryCode;
                    oktaProfile.MobilePhone = userResult.ReturnValue.Profile.MobilePhone;
                    oktaProfile.MobilePhoneCountryCode = userResult.ReturnValue.Profile.MobilePhoneCountryCode;
                    oktaProfile.CredentialsOnly = true;
                    oktaProfile.UniversalId = ttIndividualId;

                    Logger.SystemLogger.Log(string.Format("Updating Okta profile for {0}", user.LoginId));

                    IResult<User> updateResult = new Result<User>();
                    Task.Run(async () =>
                    {
                        updateResult = await identityProvider.UpdateUserProfile(oktaProfile);
                    }).GetAwaiter().GetResult();
                    
                    if (!updateResult.IsSuccess)
                    {
                        Logger.SystemLogger.Log(string.Format("Failed to update Okta profile for {0}. Error: {1}", user.LoginId, updateResult.ErrorResponse.ErrorSummary ?? updateResult.ErrorResponse.ErrorCode));

                        throw new Exception(updateResult.ErrorResponse.ErrorSummary ?? updateResult.ErrorResponse.ErrorCode);
                    }

                    Logger.SystemLogger.Log(string.Format("Updated Okta profile for {0}", user.LoginId));

                    userProfile.SyncStatus = Constants.LogStatus.Success.ToString();
                    Logger.SystemLogger.Log(string.Format("User login ID: {0} - {1}", user.LoginId, "Success"));
                }
                else
                {
                    JobSuccess = false;

                    userProfile.SyncStatus = Constants.LogStatus.Failed.ToString();
                    if (userResult.ReturnValue.Profile.CredentialsOnly)
                    {
                        userProfile.SyncDescription = Constants.USERTYPE_CREDENTIALS_ONLY;
                    }
                    else
                    {
                        userProfile.SyncDescription =
                            userResult.ErrorResponse != null ?
                                userResult.ErrorResponse.ErrorSummary ??
                                userResult.ErrorResponse.ErrorCode :
                                Constants.UNKNOWN_RESULT;
                    }

                    Logger.SystemLogger.Log(string.Format("User LoginId: {0} - {1}", user.LoginId, "Failed: " + userProfile.SyncDescription));
                }
            }
            catch (Exception ex)
            {
                JobSuccess = false;
                userProfile.SyncStatus = Constants.LogStatus.Failed.ToString();
                userProfile.SyncDescription = ex.Message;
                Logger.SystemLogger.Log(string.Format("User LoginId: {0} - {1}", user.LoginId, "Failed: " + ex.Message));
            }
            finally
            {
                //Log this record
                Database.InsertLogRecord(userProfile);

                Logger.SystemLogger.Log(string.Format("User LoginId: {0} processed", user.LoginId));
            }
        }
    }
}