namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Traveler.ProfileInfo")]
    public partial class ProfileInfo
    {
        [Key]
        public int iProfileID { get; set; }

        [StringLength(10)]
        public string nvchTitle { get; set; }

        [StringLength(10)]
        public string nvchSuffix { get; set; }

        [Required]
        [StringLength(50)]
        public string nvchFirstname { get; set; }

        [Required]
        [StringLength(50)]
        public string nvchLastname { get; set; }

        [StringLength(1)]
        public string nvchGender { get; set; }

        public int iTravelerTypeID { get; set; }

        public int? iParentcompanyid { get; set; }

        public int iInitialsourceid { get; set; }

        public Guid? unqRegistrationid { get; set; }

        [StringLength(100)]
        public string nvchPlaceofBirth { get; set; }

        public DateTime? dtBirth { get; set; }

        public int? iHomeCountryID { get; set; }

        public int? iAssignmentCountryID { get; set; }

        [StringLength(25)]
        public string nvchSalutation { get; set; }

        [StringLength(50)]
        public string nvchMiddleName { get; set; }

        [StringLength(200)]
        public string nvchComments { get; set; }

        public bool? bVIP { get; set; }

        [StringLength(100)]
        public string nvchDivision { get; set; }

        [StringLength(100)]
        public string nvchBusinessUnit { get; set; }

        [StringLength(100)]
        public string nvchHomeSite { get; set; }

        public DateTime? dtInserted { get; set; }

        public DateTime? dtModified { get; set; }

        [StringLength(50)]
        public string nvchModifiedby { get; set; }

        [StringLength(50)]
        public string nvchinsertedby { get; set; }

        public bool? iProfileId_Matched { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
