namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfileSyncLog")]
    public partial class ProfileSyncLog
    {
        [Key]
        public int SyncLogID { get; set; }

        public int SyncJobID { get; set; }

        public int LoginID { get; set; }

        public int RunNumber { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime InsertDate { get; set; }

        [StringLength(100)]
        public string OktaLoginName { get; set; }

        [StringLength(100)]
        public string OktaFirstName { get; set; }

        [StringLength(100)]
        public string OktaLastName { get; set; }

        [StringLength(100)]
        public string OktaEmailId { get; set; }

        [StringLength(50)]
        public string OktaMobilePhone { get; set; }

        [StringLength(10)]
        public string OktaCountryCode { get; set; }

        [StringLength(100)]
        public string OktaUniversalId { get; set; }

        [Required]
        [StringLength(20)]
        public string SyncStatus { get; set; }

        public string SyncDescription { get; set; }
    }
}
