namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfileCheckLog")]
    public partial class ProfileCheckLog
    {
        [Key]
        public int CheckLogID { get; set; }

        public int RunNumber { get; set; }

        public int LoginID { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(20)]
        public string MembershipNumber { get; set; }

        public bool? IsTravelTracker { get; set; }

        [StringLength(100)]
        public string IndividualId { get; set; }

        public bool? FoundInTT { get; set; }

        public bool? MatchedTT { get; set; }

        public bool? FoundInOkta { get; set; }

        public bool? MatchedOkta { get; set; }

        public string LogDescription { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime InsertDate { get; set; }
    }
}
