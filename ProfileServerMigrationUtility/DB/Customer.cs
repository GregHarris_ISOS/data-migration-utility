namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reference.Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            CustomerSettings = new HashSet<CustomerSetting>();
            //ProfileInfoes = new HashSet<ProfileInfo>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int iCustomerID { get; set; }

        [StringLength(100)]
        public string nvchName { get; set; }

        public DateTime? dtInserted { get; set; }

        public DateTime? dtModified { get; set; }

        [StringLength(50)]
        public string nvchModifiedby { get; set; }

        [StringLength(50)]
        public string nvchinsertedby { get; set; }

        [StringLength(10)]
        public string nvchWinis_Number { get; set; }

        public int iCustomerEnvironmentID { get; set; }

        public int? iSalesforceAccountId { get; set; }

        [StringLength(50)]
        public string nvchSalesforceProgramNumber { get; set; }

        public bool? bActive { get; set; }

        public bool? bShowInTravelTrackerAdmin { get; set; }

        [StringLength(200)]
        public string nvchSalesforceAcctIdModifiedBy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSetting> CustomerSettings { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<ProfileInfo> ProfileInfoes { get; set; }
    }
}
