namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfileSyncJob")]
    public partial class ProfileSyncJob
    {
        [Key]
        public int SyncJobID { get; set; }

        [Required]
        [StringLength(20)]
        public string MembershipNumber { get; set; }

        public bool ActivateMembership { get; set; }

        [Required]
        [StringLength(100)]
        public string EmailID { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime InsertDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [StringLength(20)]
        public string SyncStatus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? StartDateTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? EndDateTime { get; set; }

        public string SyncDescription { get; set; }
    }
}
