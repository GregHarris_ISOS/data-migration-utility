namespace ProfileServerMigrationUtility
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Admin.CustomerSettings")]
    public partial class CustomerSetting
    {
        [Key]
        public int iCustomerSettingsId { get; set; }

        public int iCustomerId { get; set; }

        public bool bStatus { get; set; }

        [Column("bIsDeleted ")]
        public bool bIsDeleted_ { get; set; }

        public int? iReturnToHomeCountry { get; set; }

        public int? iNotReturningToHomeCountry { get; set; }

        public double? fLayoverPeriod { get; set; }

        public bool? bIsSetupInPTL { get; set; }

        public bool bIsExpatEnabled { get; set; }

        [StringLength(50)]
        public string nvchInsertedby { get; set; }

        public DateTime? dtInserted { get; set; }

        [Required]
        [StringLength(50)]
        public string nvchModifiedby { get; set; }

        public DateTime dtModified { get; set; }

        [StringLength(50)]
        public string nvchSDMName { get; set; }

        public bool? bIsLocationTrackingEnabled { get; set; }

        [StringLength(250)]
        public string nvchComments { get; set; }

        [StringLength(50)]
        public string nvchTTMembershipNumber { get; set; }

        [StringLength(50)]
        public string nvchPortalMembershipNumber { get; set; }

        public bool? bIsPTAOnly { get; set; }

        [StringLength(50)]
        public string nvchMembershipTier { get; set; }

        [StringLength(50)]
        public string nvchFromAddress { get; set; }

        public bool? bIsOfficeLocationsEnabled { get; set; }

        public bool? bIsSOSLocationsEnabled { get; set; }

        [StringLength(50)]
        public string nvchReplyToAddress { get; set; }

        public bool? bIs1waySMSEnabled { get; set; }

        public bool? bIs2waySMSEnabled { get; set; }

        [StringLength(3)]
        public string nvchMembershipType { get; set; }

        public bool bUseTT6ProactiveEmail { get; set; }

        public bool? bUploadsHRData { get; set; }

        public DateTime? dtEarliestHRDate { get; set; }

        public bool? bUsesCustomRiskRatings { get; set; }

        public bool? bShowVIPFilter { get; set; }

        public bool? bShowTicketingFilter { get; set; }

        public bool? bCanFilterManuallyEditedTrips { get; set; }

        public bool? bIsVismoEnabled { get; set; }

        public bool? bIsPromptedCheckinEnabled { get; set; }

        public bool? bIsUberEnabled { get; set; }

        public bool bIsLoginRequiredforProactiveEmailNameDownload { get; set; }

        public bool bIsTTMSEnabled { get; set; }

        public bool bIsTTMSDefaultMessage { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
