﻿using ISOS.IdentitySDK;
using ISOS.IdentitySDK.Models;
using ISOS.IdentitySDK.Providers;
using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading;
using System.Threading.Tasks;
using ProfileServerMigrationUtility.Common;

namespace ProfileServerMigrationUtility
{
    public class TTtoIdam
    {
        internal ProfileSyncJob SyncJob { get; set; }
        private bool JobSuccess { get; set; }
        private DataService ds = new DataService();
        private MembershipInfo membershipInfo;

        public TTtoIdam(ProfileSyncJob syncjob)
        {
            SyncJob = syncjob;
        }
        public void StartJob()
        {
            Logger.SystemLogger.Log(string.Format("Migrating profiles from TT to Okta for Membership #{0}", SyncJob.MembershipNumber));

            try
            {
                List<LoginUser> loginUsers = Database.SelectLoginUser(SyncJob.MembershipNumber, SyncJob.SyncJobID);

                if (loginUsers.Any())
                {
                    Logger.SystemLogger.Log(string.Format("Found {0} profiles to migrate", loginUsers.Count));

                    JobSuccess = true;
                    //Get RunNumber from database
                    int iRun = Database.SelectRunNumber(SyncJob.SyncJobID);

                    //Update Status in database
                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.InProgress.ToString());

                    foreach (LoginUser user in loginUsers)
                    {
                        MigrateUser(user, iRun);
                    }

                    string jobReport = Database.SelectJobReport(SyncJob.SyncJobID, iRun);
                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID,
                                                    JobSuccess == true ? Constants.JobStatus.Completed.ToString() : Constants.JobStatus.CompletedWithErrors.ToString(),
                                                    jobReport);
                }
                else
                {
                    Logger.SystemLogger.Log(string.Format("No profiles to migrate for {0}", SyncJob.MembershipNumber));
                    Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.Failed.ToString(), Constants.USERS_NOT_FOUND);
                }

                Console.WriteLine("Migrating profiles complete for Membership #: {0}", SyncJob.MembershipNumber.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: {0}", ex.Message);

                Database.UpdateProfileSyncJob(SyncJob.SyncJobID, Constants.JobStatus.Failed.ToString(), ex.Message);
            }

        }

        public void MigrateUser(LoginUser user, int iRunNumber)
        {
            ProfileSyncLog userProfile = new ProfileSyncLog();
            userProfile.SyncJobID = SyncJob.SyncJobID;
            userProfile.LoginID = user.LoginId;
            userProfile.RunNumber = iRunNumber;
            userProfile.OktaLoginName = user.UserName;

            try
            {
                #region Get Profile from Okta and check if update is required
                IdentityProvider identityProvider = new IdentityProvider();
                IResult<User> userResult = new Result<User>();

                Task.Run(async () =>
                {
                    userResult = await identityProvider.GetUser(user.UserName).ConfigureAwait(false);
                }).GetAwaiter().GetResult();

                if (userResult.IsSuccess && userResult.ReturnValue.Profile.CredentialsOnly == true)
                {
                    #region Get Profile from Travel Tracker
                    GetUserProfileRequest ttProfileRequest = new GetUserProfileRequest();
                    ttProfileRequest.membershipId = SyncJob.MembershipNumber;
                    ttProfileRequest.individualId = user.IndividualId;
                    ttProfileRequest.travelTracker = true;

                    GetUserProfileResponse ttProfileResponse = new GetUserProfileResponse();
                    Task.Run(async () =>
                    {
                        ttProfileResponse = await ds.GetUserProfile(ttProfileRequest);
                    }).GetAwaiter().GetResult();
                    #endregion

                    //Update User flag in Okta
                    UserProfile oktaProfile = new UserProfile();
                    oktaProfile.Login = user.UserName;
                    oktaProfile.FirstName = ttProfileResponse.Profile.FirstName;
                    oktaProfile.LastName = ttProfileResponse.Profile.LastName;
                    oktaProfile.Email = ttProfileResponse.Profile.EmailAddress;
                    oktaProfile.CountryCode = GetCountryIsoCodeFromCountryId(ttProfileResponse.Profile.CountryOfResidence);
                    oktaProfile.MobilePhone = ttProfileResponse.Profile.MobileNumber;
                    oktaProfile.MobilePhoneCountryCode = ttProfileResponse.Profile.MobileNumberCountryCode;
                    oktaProfile.CredentialsOnly = false;
                    oktaProfile.UniversalId = user.IndividualId;
                    IResult<User> updateResult = new Result<User>();
                    Task.Run(async () =>
                    {
                        updateResult = await identityProvider.UpdateUserProfile(oktaProfile);
                    }).GetAwaiter().GetResult();

                    if (!updateResult.IsSuccess)
                    {
                        throw new Exception(userResult.ErrorResponse.ErrorSummary ?? userResult.ErrorResponse.ErrorCode);
                    }

                    userProfile.SyncStatus = Constants.LogStatus.Success.ToString();
                    Console.WriteLine("User name: {0} - {1}", user.UserName, "Success");
                }
                else
                {
                    JobSuccess = false;
                    Console.WriteLine("User name: {0} - {1}", user.UserName, "Failed");

                    userProfile.SyncStatus = Constants.LogStatus.Failed.ToString();
                    if (userResult.ReturnValue.Profile.CredentialsOnly == false)
                    {
                        userProfile.SyncDescription = Constants.USERTYPE_APP_PROFILE;
                    }
                    else
                    {
                        userProfile.SyncDescription = userResult.ErrorResponse.ErrorSummary;
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                JobSuccess = false;
                userProfile.SyncStatus = Constants.LogStatus.Failed.ToString();
                userProfile.SyncDescription = ex.Message;
                Console.WriteLine("User name: {0} - {1}", user.UserName, "Failed");
            }
            finally
            {
                //Log this record
                Database.InsertLogRecord(userProfile);
            }
        }

        private static string GetCountryIsoCodeFromCountryId(int countryId)
        {
            var countryInfo = CountryTableSelect.GetCountryInfo_By_IsosID(new string[] { countryId.ToString() });
            if (countryInfo == null || countryInfo.Count == 0)  // No matching countries
                return string.Empty;

            return countryInfo[0].ISOCountryCode;
        }

    }
}
