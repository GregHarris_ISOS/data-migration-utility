﻿using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ProfileServerMigrationUtility
{
    public static class CountryTableSelect
    {
        private static volatile DataTable _table;

        static CountryTableSelect()
        {
            _table = GetTable();
        }

        internal static List<CountryInfo> GetCountryInfo_By_IsosID(string[] isosIds)
        {
            var dr = GetTable().Select($"CountryID IN ({string.Join(",", isosIds)})");

            if (dr.Length < isosIds.Length)
            {                
                var ar = new string[dr.Length];

                for (var i = 0; i < dr.Length; i++)
                {
                    ar[i] = dr[i]["CountryID"].ToString();
                }

                foreach (var item in isosIds)
                {
                    if (Array.IndexOf(ar, item) == -1)
                    {
                        throw new Exception(
                            $"CountryTableSelect.GetCountryInfo_By_IsosID(string[] IsosId) MSG: Unknown IsosID: {item}");
                    }                    
                }
            }

            return dr.Select(item => new CountryInfo(item)).ToList();
        }

        internal static List<CountryInfo> GetCountryInfo_By_IsoCode(string[] isoCode)
        {
            var sbInString = new StringBuilder();

            foreach (var code in isoCode)
            {
                sbInString.Append("'" + code + "'");
            }

            var dr = GetTable().Select($"ISOCountryCode IN ({sbInString})");

            if (dr.Length < isoCode.Length)
            {
                var ar = new string[dr.Length];

                for (var i = 0; i < dr.Length; i++)
                {
                    ar[i] = dr[i]["ISOCountryCode"].ToString().Trim();
                }

                foreach (var item in isoCode)
                {
                    if (Array.IndexOf(ar, item) == -1)
                    {
                        throw new Exception(
                            $"CountryTableSelect.GetCountryInfo_By_IsoCode(string[] IsoCode) MSG: Unknown IsoCode: {item}");
                    }
                }
            }

            return dr.Select(item => new CountryInfo(item)).ToList();
        }

        internal static List<CountryInfo> GetCountryInfo_By_IsoCode(string isoCode)
        {
            return GetTable().Select($"CountryCode = '{isoCode}' OR ISOCountryCode = '{isoCode}'").Select(item => new CountryInfo(item)).ToList();
        }

        internal static List<CountryInfo> GetCountryInfo_By_CallingCode(int callingCode)
        {
            return GetTable().Select($"CallingCode = {callingCode}").Select(item => new CountryInfo(item)).ToList();
        }

        private static readonly object Lock = new object();

        private static DataTable GetTable()
        {
            if (_table != null) return _table;
            lock (Lock)
            {
                if (_table == null)
                {
                    _table = Database.SelectCountryTable();
                }
            }
            return _table;
        }
    }
}