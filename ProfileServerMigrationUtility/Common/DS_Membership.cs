﻿using Flurl.Http;
using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility
{
    public partial class DataService
    {
        public async Task<MembershipInfo> GetMembershipInfo(string membershipNumber)
        {

            try
            {
                MembershipInfo membership = new MembershipInfo();


                var request = new GetMembershipInfoRequest
                {
                    deviceId = 0,
                    locale = null,
                    membershipNumber = membershipNumber,
                    appVersion = null
                };

                var url = ConfigurationManager.AppSettings["WebAPIServer"] + Constants.WEBAPI_membershipInformation;


                var response = await url.PostJsonAsync(request).ReceiveJson<GetMembershipInfoResponse>();


                if (response == null)
                    throw new Exception(Constants.MEMBERSHIP_NOT_FOUND);

                if (response.StatusCode == Constants.OK)
                {
                    // Set the membership number since not returned
                    response.MembershipInfo.MembershipNumber = membershipNumber;
                    membership = response.MembershipInfo;
                    membership.StatusCode = response.StatusCode;

                    // Get the languages from the web service
                    //membership.AvailableLanguages = await GetMembershipContentLanguages(deviceId, membershipNumber);

                    return membership;

                }
                else if (response.StatusCode == Constants.MEMBERSHIP_TYPE_BLANK)
                {
                    throw new Exception(Constants.MEMBERSHIP_TYPE_BLANK);
                }
                else if (response.StatusCode == Constants.MOBILE_NOT_ENABLED)
                {
                    throw new Exception(Constants.MOBILE_NOT_ENABLED);
                }

                throw new Exception(Constants.MEMBERSHIP_NOT_FOUND);


            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

    }
}
