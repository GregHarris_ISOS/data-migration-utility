﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml;

namespace ProfileServerMigrationUtility
{
    public static class Database
    {
        private static void BeginExecuteNonQuery(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            RunNonQuery(false, connectionName, storedProcedureName, parameters);
        }

        private static void ExecuteNonQuery(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            RunNonQuery(true, connectionName, storedProcedureName, parameters);
        }
        private static void RunNonQuery(bool wait, string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                if (wait)
                    database.ExecuteNonQuery(dbCommand);
                else
                    database.BeginExecuteNonQuery(dbCommand, null, null);
            }
        }

        private static async Task<object> ExecuteScalarAsync(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                dbCommand.Connection = database.CreateConnection();
                await dbCommand.Connection.OpenAsync().ConfigureAwait(false);
                return await dbCommand.ExecuteScalarAsync().ConfigureAwait(false);
            }
        }

        private static object ExecuteScalar(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                dbCommand.Connection = database.CreateConnection();
                dbCommand.Connection.Open();
                return dbCommand.ExecuteScalar();
            }
        }

        private static IDataReader GetDataReader(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters = null)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                return database.ExecuteReader(dbCommand);
            }
        }

        private static IDataReader GetDataReader(string connectionName, string storedProcedureName)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                return database.ExecuteReader(dbCommand);
            }
        }

        private static async Task<IDataReader> GetDataReaderAsync(SqlConnection dbConnection, string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters = null)
        {
            using (var dbCommand = new SqlCommand(storedProcedureName, dbConnection))
            {
                dbCommand.CommandType = CommandType.StoredProcedure;
                if(parameters!=null)
                { 
                    foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                }
                dbConnection.ConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ToString();
                await dbConnection.OpenAsync().ConfigureAwait(false);
                return await dbCommand.ExecuteReaderAsync().ConfigureAwait(false);
            }
        }

        private static DataSet GetDataSet(string connectionName, string storedProcedureName, IEnumerable<SqlParameter> parameters)
        {
            var databaseFactory = new DatabaseProviderFactory();
            var database = databaseFactory.Create(connectionName) as SqlDatabase;
            Debug.Assert(database != null, "database != null");
            using (var dbCommand = database.GetStoredProcCommand(storedProcedureName))
            {
                foreach (var parameter in parameters)
                    dbCommand.Parameters.Add(parameter);
                return database.ExecuteDataSet(dbCommand);
            }
        }

        internal static List<ProfileSyncJob> SelectProfileSyncJob()
        {
            List<ProfileSyncJob> migrationJobs = new List<ProfileSyncJob>();
            using (var reader = GetDataReader("MobileMiddleware", "spSelectProfileSyncJob"))
            {
                while (reader.Read())
                {
                    ProfileSyncJob job = new ProfileSyncJob();
                    job.SyncJobID = Convert.ToInt32(reader["SyncJobID"]);
                    job.MembershipNumber = reader["MembershipNumber"].ToString();
                    job.ActivateMembership = Convert.ToBoolean(reader["ActivateMembership"]);
                    migrationJobs.Add(job);
                }
            }
            return migrationJobs;
        }

        //internal static async Task<List<ProfileSyncJob>> SelectProfileSyncJob()
        //{
        //    List<ProfileSyncJob> migrationJobs = new List<ProfileSyncJob>();
        //    using (var dbConnection = new SqlConnection())
        //    using (var reader = await GetDataReaderAsync(dbConnection,"MobileMiddleware", "spSelectProfileSyncJob", null))
        //    {
        //        while (reader.Read())
        //        {
        //            ProfileSyncJob job = new ProfileSyncJob();
        //            job.SyncJobID = Convert.ToInt32(reader["SyncJobID"]);
        //            job.MembershipNumber = reader["MembershipNumber"].ToString();
        //            job.ActivateMembership = Convert.ToBoolean(reader["ActivateMembership"]);
        //            migrationJobs.Add(job);
        //        }
        //    }
        //    return migrationJobs;
        //}

        internal static void UpdateProfileSyncJob(int syncJobID, string jobStatus, string jobReport = null)
        {
            var parameters = new[] { new SqlParameter("@SyncJobID", syncJobID),
                                    new SqlParameter("@SyncStatus", jobStatus),
                                    new SqlParameter("@SyncDescription", jobReport)};
            BeginExecuteNonQuery("MobileMiddleware", "spUpdateProfileSyncJobStatus", parameters);
        }

        //internal static async Task<List<LoginUser>> SelectLoginUser(string membershipNumber)
        //{
        //    List<LoginUser> loginUsers = new List<LoginUser>();
        //    var parameters = new[] { new SqlParameter("@MembershipNumber", membershipNumber) };

        //    using (var dbConnection = new SqlConnection())
        //    using (var reader = await GetDataReaderAsync(dbConnection, "MobileMiddleware", "spSelectLoginUsersByMembership ", parameters))
        //    {
        //        while (reader.Read())
        //        {
        //            LoginUser loginUser = new LoginUser();
        //            loginUser.LoginId = Convert.ToInt32(reader["LoginID"]);
        //            loginUser.UserName = reader["UserName"].ToString();
        //            loginUser.IndividualId = reader["IndividualID"].ToString();
        //            loginUsers.Add(loginUser);
        //        }
        //    }
        //    return loginUsers;
        //}

        internal static List<LoginUser> SelectLoginUser(string membershipNumber, int syncJobId)
        {
            List<LoginUser> loginUsers = new List<LoginUser>();
            var parameters = new[] { new SqlParameter("@MembershipNumber", membershipNumber),
                                    new SqlParameter("@SyncJobID", syncJobId)};

            using (var reader = GetDataReader("MobileMiddleware", "spSelectProfileSyncJobUsers", parameters))
            {
                while (reader.Read())
                {
                    LoginUser loginUser = new LoginUser();
                    loginUser.LoginId = Convert.ToInt32(reader["LoginID"]);
                    loginUser.UserName = reader["UserName"].ToString();
                    loginUser.MembershipNumber = reader["MembershipNumber"].ToString();
                    loginUser.IndividualId = reader["IndividualID"].ToString();
                    if (reader["DeviceID"] != DBNull.Value)
                        loginUser.DeviceId = Convert.ToInt32(reader["DeviceID"]);
                    loginUser.Model = reader["Model"].ToString();
                    loginUser.OSVersion = reader["OSVersion"].ToString();
                    loginUser.AppVersion = reader["AppVersion"].ToString();
                    loginUsers.Add(loginUser);
                }
            }
            return loginUsers;
        }

        internal static void InsertLogRecord(ProfileSyncLog logRecord)
        {
            try
            {
                var parameters = new[] { new SqlParameter("@SyncJobID", logRecord.SyncJobID),
                                        new SqlParameter("@LoginID", logRecord.LoginID),
                                        new SqlParameter("@RunNumber", logRecord.RunNumber),
                                        new SqlParameter("@SyncStatus", logRecord.SyncStatus),
                                        new SqlParameter("@OktaLoginName", logRecord.OktaLoginName),
                                        new SqlParameter("@OktaFirstName", logRecord.OktaFirstName),
                                        new SqlParameter("@OktaLastName", logRecord.OktaLastName),
                                        new SqlParameter("@OktaEmailId", logRecord.OktaEmailId),
                                        new SqlParameter("@OktaMobilePhone", logRecord.OktaMobilePhone),
                                        new SqlParameter("@OktaCountryCode", logRecord.OktaCountryCode),
                                        new SqlParameter("@OktaUniversalId", logRecord.OktaUniversalId),
                                        new SqlParameter("@SyncDescription", logRecord.SyncDescription),
                                        };
                BeginExecuteNonQuery("MobileMiddleware", "spInsertProfileSyncLog", parameters);
            }
            catch (Exception)
            {
                //do nothing
            }

        }
        
        //internal static async Task<int> SelectRunNumber(int syncJobId)
        //{
        //    int iRun = 0;
        //    var parameters = new[] { new SqlParameter("@SyncJobID", syncJobId) };

        //    using (var dbConnection = new SqlConnection())
        //    using (var reader = await GetDataReaderAsync(dbConnection, "MobileMiddleware", "spSelectProfileSyncLogRun", parameters))
        //    {
        //        if(reader.Read())
        //            iRun = Convert.ToInt32(reader["RunNumber"]);
        //    }
        //    return iRun;
        //}

        internal static int SelectRunNumber(int syncJobId)
        {
            int iRun = 0;
            var parameters = new[] { new SqlParameter("@SyncJobID", syncJobId) };

            using (var reader = GetDataReader("MobileMiddleware", "spSelectProfileSyncLogRun", parameters))
            {
                if (reader.Read())
                    iRun = Convert.ToInt32(reader["RunNumber"]);
            }
            return iRun;
        }

        internal static string SelectJobReport(int syncJobId, int runId)
        {
            var parameters = new[] { new SqlParameter("@SyncJobID", syncJobId), new SqlParameter("@RunId", runId) };
            return ExecuteScalar("MobileMiddleware", "spSelectProfileSyncJobReport", parameters).ToString();
        }

        internal static int SelectCountryId(string countryCode)
        {
            try
            {
                var parameters = new[] { new SqlParameter("@CountryCode", countryCode) };
                int countryId;
                countryId = Convert.ToInt32(ExecuteScalar("MobileMiddleware", "spSelectCountryID", parameters).ToString());
                return countryId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        internal static DataTable SelectCountryTable()
        {
            var parameters = new[] {
                new SqlParameter("@Return_Code", DBNull.Value),
                new SqlParameter("@Return_MSG", DBNull.Value)
            };
            return GetDataSet("MobileMiddleware", "spSelectCountryTable", parameters).Tables[0];
        }

    }
}