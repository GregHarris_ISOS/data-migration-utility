﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility
{
    public static class Constants
    {
        public const string WEBAPI_membershipInformation = "getMembershipInfo";
        public const string WEBAPI_createUserProfile = "createUserProfile";
        public const string WEBAPI_getUserProfile = "getUserProfile";

        public const string USERS_NOT_FOUND = "USERS_NOT_FOUND";
        public const string USERNAME_NOT_FOUND = "USERNAME_NOT_FOUND";
        public const string USERNAME_EXISTS = "USERNAME_EXISTS";
        public const string MEMBERSHIP_NUMBER_MISSING = "MEMBERSHIP_NUMBER_MISSING";
        public const string EMAIL_NOT_SUPPORTED = "EMAIL_NOT_SUPPORTED";
        public const string MEMBERSHIP_NUMBER_INVALID = "MEMBERSHIP_NUMBER_INVALID";
        public const string TOKEN_INVALID = "TOKEN_INVALID";
        public const string OK = "OK";
        public const string MEMBERSHIP_TYPE_BLANK = "MEMBERSHIP_TYPE_BLANK";
        public const string MOBILE_NOT_ENABLED = "MOBILE_NOT_ENABLED";
        public const string MEMBERSHIP_NOT_FOUND = "MEMBERSHIP_NOT_FOUND";

        public const string USERTYPE_CREDENTIALS_ONLY = "USERTYPE_IS_CREDENTIALS_ONLY";
        public const string USERTYPE_APP_PROFILE = "USERTYPE_IS_APP_PROFILE";
        public const string UNKNOWN_RESULT = "UNKNOWN_RESULT";

        ///<summary>E0000008: The requested path was not found</summary>
		public const string ProfileNotFoundException = "E0000200";

        ///<summary>E0000019: Profile could not be created</summary>
        public const string CreateProfileFailedException = "E0000201";

        ///<summary>E0000019: Profile could not be updated</summary>
        public const string UpdateProfileFailedException = "E0000202";

        public enum JobStatus
        {
            NotStarted,
            InProgress,
            Completed,
            CompletedWithErrors,
            Failed
        }

        public enum LogStatus
        {
            Success,
            Failed
        }

    }
}
