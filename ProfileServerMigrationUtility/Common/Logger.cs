﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISOS.SystemLoggerSDK;

namespace ProfileServerMigrationUtility.Common
{
    class Logger
    {
        private static ISOSLogger _systemLogger;
        private static string ApplicationName => ConfigurationManager.AppSettings["ApplicationName"];

        public static ISOSLogger SystemLogger
        {
            get
            {
                if (_systemLogger == null)
                {

                    string logLevel = ConfigurationManager.AppSettings["SystemLoggingLevel"];

                    _systemLogger = new ISOSLogger(ApplicationName, "SystemLogging", !string.IsNullOrEmpty(logLevel) ? logLevel : LogLevel.Trace.ToName());
                }

                return _systemLogger;
            }
        }
    }
}
