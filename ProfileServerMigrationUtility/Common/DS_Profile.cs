﻿using Flurl.Http;
using ProfileServerMigrationUtility.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileServerMigrationUtility
{
    public partial class DataService
    {
        public async Task<string> CreateUserProfile(CreateUserProfileRequest createProfileRequest)
        {
            var url = ConfigurationManager.AppSettings["WebAPIServer"] + Constants.WEBAPI_createUserProfile;

            var response = await url.PostJsonAsync(createProfileRequest).ReceiveJson<CreateUserProfileResponse>();

            if (response == null) throw new Exception(Constants.CreateProfileFailedException);

            if (!String.IsNullOrEmpty(response.IndividualID))
            {
                return response.IndividualID;

            }
            else
                throw new Exception(Constants.CreateProfileFailedException);
        }
        public async Task<GetUserProfileResponse> GetUserProfile(GetUserProfileRequest getProfileRequest)
        {
            var url = ConfigurationManager.AppSettings["WebAPIServer"] + Constants.WEBAPI_getUserProfile;
            var response = await url.PostJsonAsync(getProfileRequest).ReceiveJson<GetUserProfileResponse>();

            if (response == null) throw new Exception(Constants.ProfileNotFoundException);

            return response;
        }
    }
}
