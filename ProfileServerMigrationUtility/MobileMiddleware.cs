namespace ProfileServerMigrationUtility
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MobileMiddleware : DbContext
    {
        public MobileMiddleware()
            : base("name=MobileMiddleware")
        {
        }

        //public virtual DbSet<ProfileCheckLog> ProfileCheckLogs { get; set; }
        public virtual DbSet<ProfileSyncJob> ProfileSyncJobs { get; set; }
        //public virtual DbSet<ProfileSyncLog> ProfileSyncLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ProfileCheckLog>()
            //    .Property(e => e.UserName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileCheckLog>()
            //    .Property(e => e.MembershipNumber)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileCheckLog>()
            //    .Property(e => e.IndividualId)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileCheckLog>()
            //    .Property(e => e.LogDescription)
            //    .IsUnicode(false);

            modelBuilder.Entity<ProfileSyncJob>()
                .Property(e => e.MembershipNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ProfileSyncJob>()
                .Property(e => e.EmailID)
                .IsUnicode(false);

            modelBuilder.Entity<ProfileSyncJob>()
                .Property(e => e.SyncStatus)
                .IsUnicode(false);

            modelBuilder.Entity<ProfileSyncJob>()
                .Property(e => e.SyncDescription)
                .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaLoginName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaFirstName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaLastName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaEmailId)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaMobilePhone)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaCountryCode)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.OktaUniversalId)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.SyncStatus)
            //    .IsUnicode(false);

            //modelBuilder.Entity<ProfileSyncLog>()
            //    .Property(e => e.SyncDescription)
            //    .IsUnicode(false);
        }
    }
}
